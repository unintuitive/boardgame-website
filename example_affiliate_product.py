# -*- coding: utf-8 -*-

from .models import Session, Product, Game

session = Session()


def simple_join():
    results = session.query(Game, Product).filter(
        Game.title == Product.bgg_title).order_by(Product.amazon_title.asc()).all()
    for row in results:
        print(row[1].amazon_title, row[1].asin,
              row[0].min_playing_time, row[0].max_playing_time)


# Gets all games, all associated products, and then for each game also
# return the category
def get_all_games_and_categories():
    results = session.query(Game, Product).filter(
        Game.title == Product.bgg_title).order_by(Product.amazon_title.asc()).all()
    for row in results:
        print(row[1].amazon_title, row[1].asin,
              row[0].min_playing_time, row[0].max_playing_time)
        for game in row[0].categories:
            print("Category: " + game.category)


def get_all_games_and_mechanics():
    results = session.query(Game, Product).filter(
        Game.title == Product.bgg_title).order_by(Product.amazon_title.asc()).all()
    for row in results:
        print(row[1].amazon_title, row[1].asin,
              row[0].min_playing_time, row[0].max_playing_time)
        for game in row[0].mechanics:
            print("Mechanic: " + game.mechanic)


# Link together Games and Products through the bgg_title -> amazon_title
# SQLalchemy will automatically create the SQL joins behind the scenes
# when you call the .categories or .mechanics method. Iterate through
# those lists to produce the individual categories and mechanics.
def get_all_games_categories_and_mechanics():
    results = session.query(Game, Product).filter(
        Game.title == Product.bgg_title).order_by(Product.amazon_title.asc()).all()
    for row in results:
        print(row[1].amazon_title, row[1].asin,
              row[0].min_playing_time, row[0].max_playing_time)
        for game_categories in row[0].categories:
            print("Category: " + game_categories.category)
        for game_mechanics in row[0].mechanics:
            print("Mechanic: " + game_mechanics.mechanic)


# get_all_games_and_categories()
# get_all_games_and_mechanics()
get_all_games_categories_and_mechanics()
