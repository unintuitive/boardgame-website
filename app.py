"""
Routes and views for the flask application.
"""

from os import environ
from datetime import datetime
from flask import render_template
from flask import Flask
from flask_migrate import Migrate
from .models import *
from .config import Config
from .shared import db


app = Flask(__name__)
app.config['DEBUG'] = True
app.config.from_object(Config)
migrate = Migrate(app, db)
with app.app_context():
    db.init_app(app)


@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
        games=db.session.query(Game).join(
            Product, Game.title == Product.bgg_title).limit(10).all()
    )


@app.route('/contact')
def contact():
    """Renders the contact page."""
    return render_template(
        'contact.html',
        title='Contact',
        year=datetime.now().year,
        message='Your contact page.'
    )


@app.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.'
    )


if __name__ == '__main__':
    HOST = environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555
    app.run(HOST, PORT)
