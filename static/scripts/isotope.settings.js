var $container = $('.grid')

var $cardGrid = $($container).isotope({
    // options
    itemSelector: '.grid-item',
    percentPosition: true,
    // layoutMode: 'fitRows',
    masonry: {
        columnWidth: '.grid-sizer',
    },
    // Alphabetical sorting based on card title
    getSortData: {
        name: 'h4',
        number: '.number-mentions parseInt',
        rating: '.rating parseInt',
    },
    sortAscending: {
        name: true,
        number: false,
        rating: false
    }
});

// Filter cards based on button click 
// $('.filter-button-group').on('click', 'list-group-item', function() {
$('.filter-list a').on('click', function () {
    var filterValue = $(this).attr('data-filter');
    $cardGrid.isotope({ filter: filterValue });
});

// Sort based on multiple factors, toggle back to original order
// $('.sort-button-group').on('click', 'button', function() {
$('.sort-checkbox input').on('change', function () {
    if ($(this).hasClass('checked')) {
        $(this).removeClass('checked');
        $cardGrid.isotope({ sortBy: 'original-order' });
    } else {
        $('.sort').removeClass('checked');
        var sortValue = $(this).attr('data-sort-value');
        $cardGrid.isotope({ sortBy: sortValue });
        $(this).addClass('checked');
    }
});

$container.imagesLoaded(function () {
    $container.isotope('layout');
});