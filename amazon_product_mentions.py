# -*- coding: utf-8 -*-
import praw
import logging
import re
from datetime import datetime
from sqlalchemy.exc import IntegrityError
from sqlalchemy import cast, Integer
from shared import db
import sys
from models import Session, engine
from models import Comment, Submission, Game
import search  # See notes in search.py
import app

# Note first "import sys"  - Allows for importing modules from the parent directory
sys.path.append('..')

reddit_api = praw.Reddit(client_id='client-id-code',
                         client_secret='client-secret-code',
                         password='password', username='username',
                         user_agent='web:boardgame-python-script:0.2 (by /u/username)')

amazon_link_regex = 'https?:\/\/www.amazon.com[^\s")]+'
amazon_asin_regex = "(\/d|\/dp|\/gp\/product\/glance|\/gp\/product|\/exec\/obidos\/asin)(\/\w+)"
logging.basicConfig(filename="AmazonProductMentions.log", level=logging.DEBUG)
bg_subreddits = ['boardgames', 'boardgamedeals', 'boardgamephotos']


class AmazonProductMentions():
    def __init__(self):
        logging.debug(
            "AmazonProductMentions run at {:%B %d, %Y}".format(datetime.now()))


    def get_games(self, quantity):
        # Not entirely sure why I need app.app but the context is required.
        # Otherwise, there's no way to communicate with the Flask app db.
        with app.app.app_context():
            results = db.session.query(Game).order_by(Game.id.asc()).limit(
                quantity)

        return results


    def slice_games(self, start, stop):
        with app.app.app_context():
            results = db.session.query(Game).order_by(Game.id.asc()).slice(
                start, stop)

        return results


    def db_add_comment(self, data):
        session = Session()
        comment = Comment(data['asin'],
                          data['search_string'],
                          data['subreddit'],
                          data['permalink'],
                          data['url'],
                          data['title'],
                          data['body'],
                          data['commented_at'],
                          datetime.now(),
                          datetime.now())
        try:
            session.add(comment)
            session.commit()
        except IntegrityError:
            print('DEBUG: Duplicate comment was rolled back.')
            session.rollback()
        finally:
            session.close()


    def db_add_submission(self, data):
        session = Session()
        submission = Submission(data['asin'],
                                data['subreddit'],
                                data['permalink'],
                                data['url'],
                                data['title'],
                                data['body'],
                                data['submitted_at'],
                                datetime.now(),
                                datetime.now())
        try:
            session.add(submission)
            session.commit()
        except IntegrityError:
            print('DEBUG: Duplicate submission was rolled back.')
            session.rollback()
        finally:
            session.close()


    def extract_asin(self, url, return_list=False):
        matches = []
        asin = re.findall(amazon_asin_regex, url)
        if asin:
            for item in asin:
                extracted_asin = item[1].replace('/', '')
                matches.append(extracted_asin)
                # none of these log messages work
                # logging.debug("Extracted ASIN {extracted_asin} from URL {url}".format(extracted_asin, url))
            if return_list:
                return matches
            return matches[0]


    def api_subreddit(self, api, subreddit):
        try:
            sub = api.subreddit(subreddit)
        except Exception as e:
            # logging.debug("Reddit API error for subreddit {subreddit} : {e}".format(subreddit, e))
            print("DEBUG: " + e)
        else:
            return sub


    def api_multireddit(self, api, user, multireddit):
        try:
            multi = api.multireddit(user, multireddit)
        except Exception as e:
            # logging.debug("Reddit API error for subreddit {subreddit} : {e}".format(subreddit, e))
            print("DEBUG: " + e)
        else:
            return multi


    def fuzzy_search(self, substring, text):
        start = None
        position = 0
        for word in substring.split(' '):
            if not word:
                continue
            match = search.FuzzySearchEngine(word).search(text, pos=position)
            if not match:
                return None
            if start is None:
                start = match.start()
            position = match.end()
        return start


    # Comment methods
    def find_by_keyword_product_comments(self, api, subreddit, search_string, count=10, capture_text=False):
        sub = self.api_subreddit(api, subreddit)
        product_mentions = []
        for submission in sub.hot(limit=count):
            submission.comment_sort = 'new'
            submission.comments.replace_more(limit=0)
            for comment in submission.comments.list():  # the list function flattens the comments
                if search_string in comment.body:
                    amazon_urls = re.findall(amazon_link_regex, comment.body)
                    if amazon_urls:
                        for url in amazon_urls:
                            data = {}
                            data['search_string'] = search_string
                            data['subreddit'] = subreddit
                            data['url'] = url
                            data['title'] = comment.submission.title
                            data['permalink'] = comment.permalink
                            if capture_text is True:
                                data['body'] = comment.body_html
                            else:
                                data['body'] = None
                            data['commented_at'] = datetime.fromtimestamp(
                                comment.created_utc)
                            product_mentions.append(data)
                            self.db_add_comment(data)

        # logging.debug("find_by_keyword_product_metions: {search_string} : {subreddit} : {product_mentions}".format(search_string, subreddit, product_mentions))
        return product_mentions


    def exact_match_keyword_comments(self, api, subreddit, search_string, count=10, capture_text=True):
        sub = self.api_subreddit(api, subreddit)
        product_mentions = []
        for submission in sub.hot(limit=count):
            submission.comment_sort = 'new'
            submission.comments.replace_more(limit=0)
            for comment in submission.comments.list():
                if search_string in comment.body:
                    data = {}
                    data['search_string'] = search_string
                    data['subreddit'] = subreddit
                    data['url'] = None
                    data['asin'] = None
                    data['title'] = comment.submission.title
                    data['permalink'] = comment.permalink
                    if capture_text is True:
                        data['body'] = comment.body_html
                    else:
                        data['body'] = None
                    data['commented_at'] = datetime.fromtimestamp(
                        comment.created_utc)
                    product_mentions.append(data)
                    self.db_add_comment(data)
        return product_mentions


    def fuzzy_search_comments(self, api, subreddit, search_string, count=10, capture_text=True):
        sub = self.api_subreddit(api, subreddit)
        keyword_mentions = []
        for submission in sub.hot(limit=count):
            submission.comment_sort = 'new'
            submission.comments.replace_more(limit=0)
            for comment in submission.comments.list():
                result = self.fuzzy_search(search_string, comment.body)
                if result:
                    data = {}
                    data['subreddit'] = subreddit
                    data['url'] = None
                    data['asin'] = None
                    data['title'] = comment.submission.title
                    data['permalink'] = comment.permalink
                    if capture_text is True:
                        data['body'] = comment.body_html
                    else:
                        data['body'] = None
                    data['commented_at'] = datetime.fromtimestamp(
                        comment.created_utc)
                    data['search_string'] = search_string
                    keyword_mentions.append(data)
                    self.db_add_comment(data)
        return keyword_mentions


    # Find unique amazon links, and a reference to the comment where it was found.
    # For example, if a single comment has three AMZN links, that's three unique matches.
    def find_all_product_comments(self, api, subreddit, count=10, capture_text=False):
        sub = self.api_subreddit(api, subreddit)
        product_mentions = []
        for submission in sub.hot(limit=count):
            submission.comment_sort = 'new'
            submission.comments.replace_more(limit=0)
            for comment in submission.comments.list():  # list flattens the comments
                amazon_urls = re.findall(amazon_link_regex, comment.body)
                if amazon_urls:
                    for url in amazon_urls:
                        data = {}
                        data['search_string'] = None
                        data['subreddit'] = subreddit
                        data['url'] = url
                        data['asin'] = self.extract_asin(url)
                        data['title'] = comment.submission.title
                        data['permalink'] = comment.permalink
                        if capture_text is True:
                            data['body'] = comment.body_html
                        else:
                            data['body'] = None
                        data['commented_at'] = datetime.fromtimestamp(
                            comment.created_utc)
                        product_mentions.append(data)
                        self.db_add_comment(data)
                        # self.db_add_data(data, 'Comment')
        # logging.debug("find_all_product_comments: {subreddit} : {product_mentions}".format(subreddit, product_mentions))
        return product_mentions


    def find_all_product_multireddit_comments(self, api, user, multireddit, count=10, capture_text=False):
        sub = self.api_multireddit(api, user, multireddit)
        product_mentions = []
        for submission in sub.hot(limit=count):
            submission.comment_sort = 'new'
            submission.comments.replace_more(limit=0)
            for comment in submission.comments.list():  # list flattens the comments
                amazon_urls = re.findall(amazon_link_regex, comment.body)
                if amazon_urls:
                    for url in amazon_urls:
                        data = {}
                        data['search_string'] = None
                        data['subreddit'] = multireddit
                        data['url'] = url
                        data['asin'] = self.extract_asin(url)
                        data['title'] = comment.submission.title
                        data['permalink'] = comment.permalink
                        if capture_text is True:
                            data['body'] = comment.body_html
                        else:
                            data['body'] = None
                        data['commented_at'] = datetime.fromtimestamp(
                            comment.created_utc)
                        product_mentions.append(data)
                        self.db_add_comment(data)
                        # self.db_add_data(data, 'Comment')
        # logging.debug("find_all_product_comments: {subreddit} : {product_mentions}".format(subreddit, product_mentions))
        return product_mentions


    # Submission methods
    def fuzzy_search_submissions(self, api, subreddit, search_string, count=10, capture_text=True):
        sub = self.api_subreddit(api, subreddit)
        keyword_mentions = []
        for submission in sub.hot(limit=count):
            # Combine and search the selftext and url to cover both
            full_submission = submission.selftext + " " + submission.url
            result = self.fuzzy_search(search_string, full_submission)
            if result:
                data = {}
                data['subreddit'] = subreddit
                data['url'] = None
                data['asin'] = None
                data['title'] = submission.title
                data['permalink'] = submission.permalink
                if capture_text is True:
                    data['body'] = full_submission
                else:
                    data['body'] = None
                data['submitted_at'] = datetime.fromtimestamp(
                    submission.created_utc)
                keyword_mentions.append(data)
                self.db_add_submission(data)
        return keyword_mentions


    def find_all_product_submissions(self, api, subreddit, count=10, capture_text=False):
        sub = self.api_subreddit(api, subreddit)
        product_submissions = []
        for submission in sub.hot(limit=count):
            # Combine and search the selftext and url to cover both
            full_submission = submission.selftext + " " + submission.url
            amazon_urls = re.findall(amazon_link_regex, full_submission)
            if amazon_urls:
                for url in amazon_urls:
                    data = {}
                    data['subreddit'] = subreddit
                    data['url'] = url
                    data['asin'] = self.extract_asin(url)
                    data['title'] = submission.title
                    data['permalink'] = submission.permalink
                    if capture_text is True:
                        data['body'] = full_submission
                    else:
                        data['body'] = None
                    product_submissions.append(data)
                    data['submitted_at'] = datetime.fromtimestamp(
                        submission.created_utc)
                    self.db_add_submission(data)
                    # self.db_add_data(data, 'Submission')

        # logging.debug("find_all_product_submissions: {subreddit} : {product_submissions}".format(subreddit, product_submissions))
        return product_submissions


mentions = AmazonProductMentions()
# results = mentions.find_by_keyword_product_comments(reddit_api, 'amazonwtf', 'penis', 15)
#results = mentions.find_all_product_comments(reddit_api, 'shutupandtakemymoney', 30, True)
# results = mentions.find_all_product_submissions(
#     reddit_api, "amazonfreebies", 30, True)
#results = mentions.find_all_product_multireddit_comments(reddit_api, 'login_local', 'networking', 30, True)

# results = mentions.fuzzy_search_comments(
#     reddit_api, 'boardgames', 'Gloomhaven', 30, True)

# results = mentions.fuzzy_search_submissions(
#     reddit_api, 'boardgames', 'Eclipse', 30, True)
# for result in results:
#     for key, value in result.items():
#         print(key, value)
#         print('\n')


games = mentions.slice_games(2, 10)
for game in games:
    results = mentions.fuzzy_search_comments(
        reddit_api, 'boardgames', game.title, 50, True)
