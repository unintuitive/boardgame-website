from .shared import db
from .config import Config

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, String, Integer, Date, Table, Boolean, Numeric, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql import func

cfg = Config()

engine = create_engine(cfg.SQLALCHEMY_DATABASE_URI)
Session = sessionmaker(bind=engine)
db.Model.metadata.reflect(engine)
# The below is required before class definitions to avoid errors.
# It clears the table definitions in memory to prevent conflicts.
db.metadata.clear()


class Comment(db.Model):
    __tablename__ = 'comments'

    id = Column(Integer, primary_key=True)
    search_string = Column(String)
    asin = Column(String)
    subreddit = Column(String)
    permalink = Column(String, nullable=False)
    url = Column(String)
    title = Column(String)
    body = Column(String)
    commented_at = Column(Date)
    created_at = Column(Date, server_default=func.now())
    updated_at = Column(Date, server_onupdate=func.now())

    def __init__(self, search_string, asin, subreddit, permalink, url, title, body, commented_at, created_at, updated_at):
        self.search_string = search_string
        self.asin = asin
        self.subreddit = subreddit
        self.permalink = permalink
        self.url = url
        self.title = title
        self.body = body
        self.commented_at = commented_at
        self.created_at = created_at
        self.updated_at = updated_at


class Category(db.Model):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    category = Column(String)
    created_at = Column(Date, server_default=func.now())
    updated_at = Column(Date, server_onupdate=func.now())

    def __init__(self, category, created_at, updated_at):
        self.category = category
        self.created_at = created_at
        self.updated_at = updated_at


# TODO: Need to fix the created_at and updated_at fields

# Junction table definition
game_category = Table('game_category', db.Model.metadata,
                      Column('game_id', Integer, ForeignKey('games.id')),
                      Column('category_id', Integer,
                             ForeignKey('categories.id')),
                      extend_existing=True)  # Extend existing required to avoid error


game_mechanic = Table('game_mechanic', db.Model.metadata,
                      Column('game_id', Integer, ForeignKey('games.id')),
                      Column('mechanic_id', Integer,
                             ForeignKey('mechanics.id')),
                      extend_existing=True)  # Extend existing required to avoid error


class Game(db.Model):
    __tablename__ = 'games'

    id = Column(Integer, primary_key=True)
    title = Column(String)
    year = Column(String)
    min_age = Column(Integer)
    min_playing_time = Column(Integer)
    max_playing_time = Column(Integer)
    min_players = Column(Integer)
    max_players = Column(Integer)
    videos = Column(String)
    expansion = Column(Boolean, default=False)
    expands = Column(String)
    expansions = Column(String)
    artists = Column(String)
    designers = Column(String)
    publishers = Column(String)
    implementations = Column(String)
    families = Column(String)
    bgg_rank = Column(Integer)
    categories = relationship(
        'Category', secondary=game_category, backref=backref('game'))
    mechanics = relationship(
        'Mechanic', secondary=game_mechanic, backref=backref('game'))

    def __init__(self,
                 title,
                 year,
                 categories,
                 mechanics,
                 min_age,
                 min_playing_time,
                 max_playing_time,
                 min_players,
                 max_players,
                 videos,
                 expansion,
                 expands,
                 expansions,
                 artists,
                 designers,
                 publishers,
                 implementations,
                 families,
                 bgg_rank):
        self.title = title
        self.year = year
        self.categories = categories
        self.mechanics = mechanics
        self.min_age = min_age
        self.min_playing_time = min_playing_time
        self.max_playing_time = max_playing_time
        self.min_players = min_players
        self.max_players = max_players
        self.videos = videos
        self.expansion = expansion
        self.expands = expands
        self.expansions = expansions
        self.artists = artists
        self.designers = designers
        self.publishers = publishers
        self.implementations = implementations
        self.families = families
        self.bgg_rank = bgg_rank


class Mechanic(db.Model):
    __tablename__ = 'mechanics'

    id = Column(Integer, primary_key=True)
    mechanic = Column(String)
    created_at = Column(Date, server_default=func.now())
    updated_at = Column(Date, server_onupdate=func.now())

    def __init__(self, mechanic, created_at, updated_at):
        self.mechanic = mechanic
        self.created_at = created_at
        self.updated_at = updated_at


class Product(db.Model):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True)
    bgg_title = Column(String)
    match_confidence = Column(Integer)
    amazon_title = Column(String)
    asin = Column(String, unique=True, nullable=False)
    studio = Column(String)
    genre = Column(String)
    price = Column(Numeric(10, 2))
    image = Column(String)
    description = Column(String)
    is_active = Column(Boolean, unique=False, default=True, nullable=False)
    verified_at = Column(Date, server_default=func.now())
    created_at = Column(Date, server_default=func.now())
    updated_at = Column(Date, server_onupdate=func.now())
    games = relationship(Game, backref='product',
                         primaryjoin='Game.title == Product.bgg_title', foreign_keys='Game.title')

    def __init__(self,
                 bgg_title,
                 match_confidence,
                 amazon_title,
                 asin,
                 studio,
                 genre,
                 price,
                 image,
                 description,
                 is_active,
                 verified_at,
                 created_at,
                 updated_at):
        self.bgg_title = bgg_title
        self.match_confidence = match_confidence
        self.amazon_title = amazon_title
        self.asin = asin
        self.studio = studio
        self.genre = genre
        self.price = price
        self.image = image
        self.description = description
        self.is_active = is_active
        self.verified_at = verified_at
        self.created_at = created_at
        self.updated_at = updated_at


class Submission(db.Model):
    __tablename__ = 'submissions'

    id = Column(Integer, primary_key=True)
    search_string = Column(String)
    asin = Column(String)
    subreddit = Column(String)
    permalink = Column(String, unique=True, nullable=False)
    url = Column(String)
    title = Column(String)
    body = Column(String)
    submitted_at = Column(Date)
    created_at = Column(Date, server_default=func.now())
    updated_at = Column(Date, server_onupdate=func.now())

    def __init__(self, search_string, asin, subreddit, permalink, url, title, body, submitted_at, created_at, updated_at):
        self.asin = asin
        self.search_string = search_string
        self.subreddit = subreddit
        self.permalink = permalink
        self.url = url
        self.title = title
        self.body = body
        self.submitted_at = submitted_at
        self.created_at = created_at
        self.updated_at = updated_at

