# -*- coding: utf-8 -*-
from datetime import date
from datetime import datetime

from models import Session, engine, Comment, Submission, Product

from sqlalchemy.exc import IntegrityError
import sys
path = "path/to/bg"
if path not in sys.path:
    sys.path.append(path)

try:
    Base.metadata.create_all(engine)
    print("Successfully created tables.")
except Exception as e:
    print(e)


# session = Session()

# comment = Comment('B01n5erpej',
#                   'r/shutupandtakemymoney',
#                   'r/shutupandtakemymoney/comments/blah/waterproof_blah/blah/',
#                   'https://amazon.com/blah/de/dah',
#                   'My reddit comment about products',
#                   'here is my comment text',
#                   datetime.now(),
#                   datetime.now(),
#                   datetime.now())

# product = Product("Temp Product Title",
#                   "B01N5ERPEJ",
#                   True,
#                   None, #null verified_at date
#                   datetime.now(),
#                   datetime.now())

# try:
#     session.add(product)
# except IntegrityError:
#     print("ERROR: Duplicate product - ASIN already present in products table.")
#     session.rollback()
# finally:
#     session.commit()
#     session.close()

# try:
#     session.add(comment)
# except Exception as e:
#     print(e)
#     session.rollback()
# finally:
#     session.commit()
#     session.close()

# product.comments = [comment]
