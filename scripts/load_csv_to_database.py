import sqlalchemy
from sqlalchemy import create_engine
import pandas

engine = create_engine('postgresql://postgres:postgres@localhost:5432/postgres')


def load_csv(db_engine, file_name, table_name, column_types):
    try:
        data_frame = pandas.read_csv(file_name,
                                     sep='\t',
                                     lineterminator='\r',
                                     error_bad_lines=False)
        data_frame.to_sql(table_name, engine,
                          if_exists='append',  # Or append!
                          dtype=column_types, index=False, index_label='id')
        print(f"Loaded data from {file_name} into table '{table_name}.'")
    except Exception as e:
        print(e)


bgg_field_map = {'bgg_id': sqlalchemy.Integer(),
                 'title': sqlalchemy.String(),
                 'year': sqlalchemy.Integer(),
                 'categories': sqlalchemy.String(),
                 'mechanics': sqlalchemy.String(),
                 'min_age': sqlalchemy.Integer(),
                 'min_playing_time': sqlalchemy.Integer(),
                 'max_playing_time': sqlalchemy.Integer(),
                 'min_players': sqlalchemy.Integer(),
                 'max_players': sqlalchemy.Integer(),
                 'videos': sqlalchemy.String(),
                 'expansion': sqlalchemy.Boolean(),
                 'expands': sqlalchemy.String(),
                 'expansions': sqlalchemy.String(),
                 'artists': sqlalchemy.String(),
                 'designers': sqlalchemy.String(),
                 'publishers': sqlalchemy.String(),
                 'implementation': sqlalchemy.String(),
                 'families': sqlalchemy.String(),
                 'bgg_ran': sqlalchemy.Integer(),
                 }


product_field_map = {
    'bgg_title': sqlalchemy.String(),
    'match_confidence': sqlalchemy.Integer(),
    'amazon_title': sqlalchemy.String(),
    'asin': sqlalchemy.String(),
    'studio': sqlalchemy.String(),
    'genre': sqlalchemy.String(),
    'price': sqlalchemy.Numeric(10, 2),
    'image': sqlalchemy.String(),
    'description': sqlalchemy.String(),
    'is_active': sqlalchemy.Boolean(),
    'verified_at': sqlalchemy.Date(),
}


load_csv(engine, 'amazon_products-alt.txt', 'products', product_field_map)

# load_csv(engine, 'bgg_top_500.tsv', 'games', bgg_field_map)
