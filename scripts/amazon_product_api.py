# -*- coding: utf-8 -*-

import bottlenose
import csv
import time
import os.path
import re


def initialize_api():
    access_key = 'amazon-access-key'
    secret_key = 'amazon-secret-key'
    aws_tag = 'amazon-tag'
    return bottlenose.Amazon(access_key, secret_key, aws_tag, MaxQPS=0.9)


def amazon_item_search(api, search_term):
    try:
        response = api.ItemSearch(
            Keywords=search_term, ResponseGroup="Medium", SearchIndex="Toys")
    except Exception as e:
        print("Amazon API Error: " + e)
    finally:
        return response


def write_xml_file(title, data):
    save_path = 'path/to/output'
    # Reformat the title to remove non-alphanumeric characters
    reformatted_title = "".join([c for c in title if re.match(r'\w', c)])
    full_file_path = os.path.join(save_path, reformatted_title + '.xml')
    try:
        # Amazon's API returns XML in bytes. Need the wb flag to write binary.
        with open(full_file_path, 'wb') as xml_file:
            xml_file.write(data)
    except Exception as e:
        print("Error writing XML: " + e)
    finally:
        print(f"Wrote XML file for {title}.")


def open_input_file(file_name):
    lines = []
    try:
        # Opening the csv file without utf-8 encoding results in errors
        with open(file_name, 'r', encoding='utf-8') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter='\t')
            for row in csv_reader:
                # Appends the cells in the title column at index 2
                lines.append(row[2])
    except Exception as e:
        print("Error opening input file: " + e)
    return lines


def search_amazon_for_game_titles(api, titles):
    # def search_amazon_for_game_titles(titles):
    # Skip the first row which contains column headers
    # Slice the array to skip 0 index, and return only the first row
    for title in titles[1:]:
        # for title in titles[1:]:
        response = amazon_item_search(api, title + ' board game')
        print(f'Searching Amazon for {title} board game')
        time.sleep(10)
        write_xml_file(title, response)


# Initialize the Amazon API object
amazon = initialize_api()
# Open the input file
game_titles = open_input_file('bgg_top_500.txt')
# Search each title with the Amazon APi
search_amazon_for_game_titles(amazon, game_titles)
