# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from os import listdir
import os.path
from os.path import isfile, join
import csv
from datetime import datetime
from models import Session, engine, Product
from sqlalchemy.exc import IntegrityError

# Next step for this scripts:
# This needs to be integrated with SQLAlchemy to import data into Postgres
# Continuing to use CSVs for this will become unwieldy.
# Maybe I can toggle storing data in the DB on and off with a boolean.
# That way I can first examine the output in Excel before storing data.


# Open the XML file, process it with BS4, and return the BS4-enhanced object
def bs4_from_xml(file_path, file_name):
    full_file_path = os.path.join(file_path, file_name)
    try:
        # Opening the csv file without utf-8 encoding results in errors
        with open(full_file_path, 'r', encoding='utf-8') as xml_file:
            soup = BeautifulSoup(xml_file, 'xml')
    except Exception as e:
        print("Error opening input file: " + e)
    return soup


# From the input file, get the original BGG titles from column three
def titles_from_csv(file_name):
    lines = []
    try:
        # Opening the csv file without utf-8 encoding results in errors
        with open(file_name, 'r', encoding='utf-8') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter='\t')
            for row in csv_reader:
                # Appends the cells in the title column at index 2
                lines.append(row[2])
    except Exception as e:
        print("Error opening input file: " + e)
    return lines


# Return a list of files only from the directory
def get_file_list(path):
    files_only = [f for f in listdir(path) if isfile(join(path, f))]
    return files_only


def write_file(data):
    with open('amazon_products.txt', 'a', encoding='utf-8') as txt_file:
        txt_file.write(data)
        txt_file.write('\n')


# Using an exact match of the title, find the item grandparent and return it
# Note that capitalized XML node names are required
def get_item_by_title(xml_name, title):
    matched_title = xml_name.find_all('Title', string=title)
    # ItemAttributes is the Title's parent
    item_attributes = matched_title[0].find_parents(
        'ItemAttributes')
    # Item is the ItemAttribute's parent
    item = item_attributes[0].find_parents(
        'Item')
    return item[0]


# Search a Soup object to match an XML Title node to the input title name
def match_amazon_title_to_bgg_title(beautiful_soup_object, bgg_title):
    bs4_titles = [beautiful_soup_object.find_all('Title')]
    amazon_titles = [r.string for r in bs4_titles[0]]
    match = process.extractOne(bgg_title, amazon_titles)
    return match


# Match the original BGG game title to the corresponding XML file name
def match_bgg_title_to_xml(game_title, sorted_files):
    extracted = process.extractOne(game_title, sorted_files)
    print(f"Matched '{game_title}' title to {extracted[0]} file.")
    return game_title, extracted[0], extracted[1]


def get_product_details_from_item(item, bgg_title):
    product_details_list = []
    # Item is of type "tag" and so must be converted to str
    xml_item = '<?xml version="1.0" ?>' + str(item)
    soup = BeautifulSoup(xml_item, 'xml')
    product_details_list.append(bgg_title)
    product_details_list.append(soup.ASIN.string)
    product_details_list.append(soup.Title.string)
    product_details_list.append(soup.Brand.string if soup.Brand else None)
    product_details_list.append(soup.Genre.string if soup.Genre else None)
    product_details_list.append(soup.FormattedPrice.string)
    product_details_list.append(
        soup.LargeImage.URL.string if soup.LargeImage else None)
    product_details_list.append(soup.Content.string.replace('\n', ' ').replace('\r', '')
                                if soup.Content else None)
    # product_details_list.append(
    #     soup.ReleaseDate.string if soup.ReleaseDate else None)
    return product_details_list

#     <LargeImage><Height>
#     <LargeImage><Width>
#     <ItemAttributes><Feature> (3 or 4 of these)


# Write a tab delimited output file
def output_item_txt(product_details_list):
    title = product_details_list[0]
    txt_line = '\t'.join(str(r) for r in product_details_list)
    write_file(txt_line)
    print(f"Wrote line for board game: {title}")


def associate_item_list_to_product(item):
    product_data = {}
    product_data['bgg_title'] = item[0]
    product_data['amazon_title'] = item[2]
    product_data['asin'] = item[1]
    product_data['studio'] = item[3]
    product_data['genre'] = item[4]
    if item[5] == 'Too low to display':
        product_data['price'] = 0.0
    else:
        product_data['price'] = item[5].replace('$', '')
    product_data['image'] = item[6]
    product_data['description'] = item[7]
    product_data['is_active'] = True
    product_data['verified_at'] = datetime.now()
    product_data['created_at'] = datetime.now()
    product_data['updated_at'] = datetime.now()
    product_data['match_confidence'] = item[8]
    return product_data


def db_add_product(product_data):
    session = Session()
    product = Product(product_data['bgg_title'],
                      product_data['match_confidence'],
                      product_data['amazon_title'],
                      product_data['asin'],
                      product_data['studio'],
                      product_data['genre'],
                      product_data['price'],
                      product_data['image'],
                      product_data['description'],
                      product_data['is_active'],
                      product_data['verified_at'],
                      product_data['created_at'],
                      product_data['updated_at'])
    try:
        session.add(product)
        session.commit()
        print(f"Wrote DB entry for {product_data['bgg_title']}")
    except IntegrityError:
        print('Duplicate product rolled back.')
        session.rollback()
    finally:
        session.close()


def produce_amazon_items(xml_file_path, csv_input_file):
    # 1. Get a list of all XML files in the directory
    file_list = get_file_list(xml_file_path)
    # 2. Get a list of all titles from the .csv file
    bgg_title_list = titles_from_csv(csv_input_file)
    sorted_titles = sorted(bgg_title_list[1:], key=str.lower)  # Skip headers
    sorted_files = sorted(file_list, key=str.lower)
    for bgg_title in sorted_titles:
        # 3. Get the matching .xml file based on the title name
        title, xml_file, confidence = match_bgg_title_to_xml(
            bgg_title, sorted_files)
        # 4. Get serchable bs4 objects from the xml file
        bs4_file = bs4_from_xml(xml_file_path, xml_file)
        # 5. Search that .xml file for item with the exact title
        matched_title = match_amazon_title_to_bgg_title(bs4_file, title)
        # 6. Return the full item based on that matched title
        item = get_item_by_title(bs4_file, matched_title)
        # 7. Acquire the desired individual details and output them to txt file
        product_details_list = get_product_details_from_item(item, bgg_title)
        # Prepend match confidence factor to list
        product_details_list.append(confidence)
        # 8. Save the item output to a tab delimited document
        output_item_txt(product_details_list)
        # 9. Add product data to the database
        product = associate_item_list_to_product(product_details_list)
        # print(product)
        db_add_product(product)


produce_amazon_items('path/to/output', 'bgg_top_500.txt')
