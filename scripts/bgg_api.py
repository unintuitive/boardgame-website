# -*- coding: utf-8 -*-
from boardgamegeek import BGGClient

# 3 retries on a HTTP 202 Retry, wait 6 seconds between retries, 9 requests
# per request. 5 second wait between requests is recommended to avoid
# throttling by the API server.
bgg = BGGClient(timeout=15, retries=3, retry_delay=6, requests_per_minute=6)

games_list = open('bgg_games_401-500.csv', 'r')
lines = games_list.readlines()
games_list.close()

lines = [line.strip() for line in lines]

# games_list = ['Gloomhaven', 'Pandemic Legacy: Season 1',
#               'Through the Ages: A New Story of Civilization',
#               'Twilight Struggle']


def bgg_api_call(game_name):
    results = []
    txt_line = ''
    result = bgg.game(game_name)
    results.append(game_name)
    results.append(result.id)
    results.append(result.name)
    # results.append(result.alternative_names)
    # results.append(result.description)
    results.append(result.year)
    results.append(result.categories)
    results.append(result.mechanics)
    results.append(result.min_age)
    results.append(result.min_playing_time)
    results.append(result.max_playing_time)
    results.append(result.playing_time)
    results.append(result.min_players)
    results.append(result.max_players)
    results.append(result.videos)
    results.append(result.expansion)
    results.append(result.expands)
    results.append(result.expansions)
    results.append(result.artists)
    results.append(result.designers)
    results.append(result.publishers)
    results.append(result.implementations)
    results.append(result.families)
    results.append(result.bgg_rank)
    # Join expects strings only, not ints and etc. Convert them before join.
    # Join with tabs
    txt_line = '\t'.join(str(r) for r in results)
    return txt_line


def write_file(data):
    with open('bgg_top_500.txt', 'a', encoding='utf-8') as txt_file:
        txt_file.write(data)
        txt_file.write('\n')


for game_name in lines:
    results = bgg_api_call(game_name)
    write_file(results)
    print(f"Wrote line to file for {game_name}.")
